<?php

namespace TYPOworx\Sitemap\Contracts;

use TYPOworx\Sitemap\Tags\Url;

interface Sitemapable
{
    public function toSitemapTag(): Url | string | array;
}
